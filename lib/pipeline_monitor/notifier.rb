module PipelineMonitor
  class Notifier
    def initialize
      if /linux/ =~ RUBY_PLATFORM
        @noty = Libnotify.new
      else
        @noty = TerminalNotifier
      end

      notify("Pipelines",
        "Começando a acompanhar os pipelines",
        "emblem-shared-symbolic",
        :low,
        2.5)
    end

    def notify(title, body, icon, urgency, timeout)
      if /linux/ =~ RUBY_PLATFORM
        @noty.update(body: body) do |notify|
          notify.summary = title
          notify.icon_path = icon
          notify.urgency = urgency
          notify.timeout = timeout
        end
      else
        @noty.notify(body, title: title)
      end
    end
  end
end
