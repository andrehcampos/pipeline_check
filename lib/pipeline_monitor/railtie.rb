require 'pipeline_monitor'
require 'rails'
module MyPlugin
  class Railtie < Rails::Railtie
    railtie_name :pipeline_monitor

    rake_tasks do
      load "tasks/pipeline_monitor.rake"
    end
  end
end
