require "pipeline_monitor/version"

module PipelineMonitor
  require 'pipeline_monitor/railtie' if defined?(Rails)
  require 'pipeline_monitor/notifier'
  # Your code goes here...
  class Engine < Rails::Engine
    initializer 'static_assets.load_static_assets' do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor"
    end
  end
end
