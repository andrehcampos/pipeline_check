module PipelineCheck
  class MonitorController < ActionController::Base
    layout 'monitor'
    def index
      @pipelines = Pipeline.order('id desc')
    end
  end
end
