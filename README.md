# PipelineMonitor

This gem allows you to monitor pipeline statuses.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'pipeline_monitor'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install pipeline_monitor

## Usage

```ruby
bundle exec rake pipeline:monitor
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/pipeline_monitor/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
